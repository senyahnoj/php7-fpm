FROM debian:stretch

# Environment variables
ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL en_GB.UTF-8
ENV LANGUAGE en_GB:en

# Update package cache
RUN apt-get update

# Install tools for package management/locales etc.
RUN apt-get -y --no-install-recommends install \
	wget \
	lsb-release \
	ca-certificates \
	locales \
	apt-utils \
	gnupg \
	apt-transport-https

# Set locale
RUN echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen && locale-gen en_GB.UTF-8 && /usr/sbin/update-locale LANG=en_GB.UTF-8

# Source the Sury PHP repository
RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
	sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list' && \
	apt-get update

# Install PHP (7.1)
RUN apt-get -y --no-install-recommends install \
	php-mysql \
	php-mcrypt \
	php-curl \
	php-intl \
	php-gettext \
	php-json \
	php-geoip \
	php-gd \
	php-imagick \
	php-xdebug \
	php-xmlrpc \
	php-zip \
	php-fpm \
	php-memcached

# Clean APT cache to save a bit of disk space
RUN apt-get autoclean && apt-get clean && apt-get autoremove

# Make php-fpm listen on port 9000
RUN sed -i -E "s/listen = .*/listen = [::]:9000/" /etc/php/7.1/fpm/pool.d/www.conf
RUN sed -i -E "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.1/fpm/php-fpm.conf

# Start PHP-FPM service
ENTRYPOINT service php7.1-fpm start && bash

# Expose port 9000
EXPOSE 9000
